package com.example.keepconnecting;

import java.util.concurrent.TimeUnit;

import com.google.inject.Inject;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.PostLoginEvent;
import com.velocitypowered.api.event.player.KickedFromServerEvent;
import com.velocitypowered.api.event.player.KickedFromServerEvent.DisconnectPlayer;
import net.kyori.adventure.text.TranslatableComponent;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.event.player.KickedFromServerEvent.RedirectPlayer;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import java.util.HashSet;

import org.slf4j.Logger;

@Plugin(id = "keep-connecting", name = "Keep Connecting", version = "0.1.0",
        url = "https://gitlab.com/m-server/velocity-keep-connecting", description = "Keep trying to connect to the server if it is unavailable, give a custom message after the timeout on failure.", authors = {"nielznl"})
public class KeepConnecting {

    private final ProxyServer server;
    private final Logger logger;

    HashSet<Player> joiningPlayers = new HashSet<>();

    @Inject
    public KeepConnecting(ProxyServer server, Logger logger) {
        this.server = server;
        this.logger = logger;
        logger.info("Players will keep connecting for a little while until the server is up.");
    }

    @Subscribe
    public void onPostLoginEvent(PostLoginEvent event) {
        Player player = event.getPlayer();
        this.joiningPlayers.add(player);
        server.getScheduler()
                .buildTask(this, () -> this.joiningPlayers.remove(player))
                .delay(20L, TimeUnit.SECONDS)
                .schedule();
    }

    @Subscribe
    public void onKickedFromServerEvent(KickedFromServerEvent event) {
        Player player = event.getPlayer();
        if (event.getResult() instanceof DisconnectPlayer disconnectPlayer && event.getServerKickReason().isEmpty() && !event.kickedDuringServerConnect()) {
            if (disconnectPlayer.getReasonComponent() instanceof TranslatableComponent translatableComponent) {
                if (translatableComponent.key().equals("velocity.error.connecting-server-error")){
                    final TextComponent textComponent = Component
                            .text("Server should be running within a couple of minutes.", NamedTextColor.BLUE)
                            .decoration(TextDecoration.BOLD, true);

                    boolean timeExceeded = !this.joiningPlayers.contains(player);
                    if (timeExceeded) {
                        //logger.info(player.toString() + " Timeout reached, disconnecting player with custom message");
                        DisconnectPlayer result = DisconnectPlayer.create(textComponent);
                        event.setResult(result);
                    } else {
                        //logger.info(player.toString() + " Keep trying to connect until timeout is reached");
                        RegisteredServer registeredServer = event.getServer();
                        RedirectPlayer redirectPlayer = RedirectPlayer.create(registeredServer, textComponent);
                        event.setResult(redirectPlayer);
                    }
                    try
                    {
                        Thread.sleep(1000);
                    }
                    catch(InterruptedException ex)
                    {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
    }
}